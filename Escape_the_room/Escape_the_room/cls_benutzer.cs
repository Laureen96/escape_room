﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Escape_the_room
{
    class cls_benutzer
    {
        int m_id;
        string m_name;
        string m_charakter;
        DateTime m_zeit;

        public string Name { get => m_name; set => m_name = value; }
        public string Charakter { get => m_charakter; set => m_charakter = value; }
        public DateTime Zeit { get => m_zeit; set => m_zeit = value; }

        public cls_benutzer(string name, string charakter, DateTime zeit)
        {
            m_name = name;
            m_charakter = charakter;
            m_zeit = zeit;
        }
    }
}
