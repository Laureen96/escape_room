﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Net.Mime.MediaTypeNames;

namespace Escape_the_room
{
    public partial class Form1 : Form
    {
        private string text;
        private string texttwo;
        private int numtwo = 0;
        private int num = 0;
        public Form1()
        {
            InitializeComponent();
        }

        private void T_timer_text_Tick(object sender, EventArgs e)
        {
            if (num < text.Length)
            {
                lab_text.Text = lab_text.Text + text.ElementAt(num);
                num++;
            }
            else
            {
                var i = Task.Delay(3000);
                lab_text.Visible = false;
                lab_text2.Visible = true;
            }

            if (lab_text2.Visible == true)
            {

                if (numtwo < texttwo.Length)
                {
                    lab_text2.Text = lab_text2.Text + texttwo.ElementAt(numtwo);
                    numtwo++;
                }
                else
                {
                    T_timer_text.Stop();
                    lab_text2.Visible = false;

                    Close();

                    frm_home home = new frm_home();
                    home.ShowDialog();
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            FormBorderStyle = FormBorderStyle.None;
            WindowState = FormWindowState.Maximized;

            text = lab_text.Text;
            lab_text.Text = "";

            texttwo = lab_text2.Text;
            lab_text2.Text = "";

            T_timer_text.Start();
        }
    }
}
