﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Windows.Forms;

namespace Escape_the_room
{
    class cls_DataProvider
    {
        const string connectionString = "datasource=localhost;port=3306;username=root;password=;database=db_escaperoom;";
        public static void InsertData(cls_benutzer benutzer)
        {
            MySqlConnection conn = new MySqlConnection(connectionString);
            string query = "INSERT INTO tbl_benutzer(name, charakter, zeit) VALUES (@name, @charakter, @zeit)";
            MySqlCommand cmd = new MySqlCommand(query, conn);
            cmd.CommandTimeout = 60;
            try
            {
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            catch
            {
                MessageBox.Show("Fehler bei der Datenbankverbindung!");
                conn.Close();
            }
        }
    }
}
